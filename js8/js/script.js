//модель документа, которую создает браузер в памяти комп ютера основываясь на нтмл код
// innerhtml мы получаем содержимое вместе с тегами нтмл , innertext мы получим только текст, без тегов
//qertySelector(All), getElementById, getElementsByClassName, getElementsByTagName и getElementsByName.
// Лучшие - quertySelectorAll и quertySelector



//1
const allParagrafs =  document.querySelectorAll("p");
allParagrafs.forEach( item => {
    item.style.color = "#ff0000"
});


//2
const options = document.getElementById("optionsList");
console.log(options);
const parentOptions = options.parentElement;
console.log(parentOptions);
const previosOptions = options.previousElementSibling;
console.log(previosOptions);


//3
const textParagraph = document.getElementById("testParagraph");
textParagraph.innerHTML = `This is a paragraph`;

//4

const elemInMainHeader = document.querySelector('.main-header');

for (item of elemInMainHeader.children) {
    item.classList.add('nav-item')
}
console.log(elemInMainHeader);


//5
const elemSectionTitle = document.querySelector('.section-title');
elemSectionTitle.remove();

