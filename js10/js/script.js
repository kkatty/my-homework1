

const tabs = document.querySelector('.tabs');

tabs.addEventListener('click', (e) => {
  let previousActiveTarget = document.querySelector('.tabs .active');
  if (e.target !== e.currentTarget && e.target.classList.contains('tabs-title')) {
    previousActiveTarget.classList.remove('active');
    document.querySelector('.tabs-content .show').classList.remove('show');
    e.target.classList.add('active');
    document.querySelector(`[data-text="${e.target.innerText.toLowerCase()}"]`).classList.add('show');
  }
})

function openCity() {

    let i, tabsContent ;

 tabsContent = document.querySelector('.tabs-content');
    for (i = 0; i < tabsContent.length; i++) {
        tabsContent[i].style.display = "none";
    }
    tabs = document.querySelector(".tabs");
    for (i = 0; i < tabs.length; i++) {
        tabs[i].className = tabs[i].className.replace(" active", "");
    }}