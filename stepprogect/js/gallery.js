
const galleryBox = document.querySelector(".gallery-box");
const imagesGallery = document.querySelectorAll(".gallery-card");
const btnGallery = document.querySelector(".gallery-button");
const hiddenImg = document.querySelectorAll(".gallery-box-hide");
const galleryLoader = document.querySelector(".gallery-loader");

galleryLoader.style.display = 'none';

btnGallery.addEventListener('click', () => {
    galleryLoader.style.display = 'inline-block';
   setTimeout(() => {
       galleryLoader.style.display = 'none';
   hiddenImg.forEach(elem => {
           elem.style.display = 'block';
      })
       galleryBox.style.height = '1950px'
    }, 2000)

       btnGallery.remove();
 })

