
let slider = document.querySelector(".carousel_img");


slider.addEventListener("click", function (ev) {
    if (ev.target.classList.contains("slider_img")) {
        document.querySelector(".carousel_img img.active").classList.remove("active");
        document.querySelector(".testimonial.active").classList.remove("active");
        document.querySelector(`#${ev.target.dataset.imgid}`).classList.add("active");
        ev.target.classList.add("active");

    }
});


const imgLength = document.querySelectorAll(".slider_img").length;
let imagesSlider = document.querySelectorAll(".carousel_img img");
document.querySelector(".border-previous-button").style.display = "none";
document.querySelector(".arrowLeft").style.display = "none";

document.querySelector(".border-previous-button").addEventListener("click", function (ev) {
    for (let i = 0; i < imagesSlider.length; i++) {
        if (imagesSlider[i].classList.contains("active")) {
            imagesSlider[i].classList.remove("active");
            imagesSlider[i - 1].classList.add("active");
            document.querySelector(".testimonial.active").classList.remove("active");
            document.querySelector(`.testimonial:nth-child(${i})`).classList.add("active");
            toggleArrows(i - 1);
            return;

        }

    }
});


document.querySelector(".border-next-button").addEventListener("click", function (ev) {
    for (let i = 0; i < imagesSlider.length; i++) {
        if (imagesSlider[i].classList.contains("active")) {
            imagesSlider[i].classList.remove("active");
            imagesSlider[i + 1].classList.add("active");
            document.querySelector(".testimonial.active").classList.remove("active");
            document.querySelector(`.testimonial:nth-child(${i + 2})`).classList.add("active");
            toggleArrows(i + 1);
            return;
        }
    }
});


function toggleArrows(i) {
    if (i === 0) {
        document.querySelector(".arrowLeft").style.display = "none";
        document.querySelector(".arrowRight").style.display = "block";
        document.querySelector(".border-previous-button").style.display = "none";
        document.querySelector(".border-next-button").style.display = "block";
    } else if (i === imgLength - 1) {
        document.querySelector(".arrowRight").style.display = "none";
        document.querySelector(".arrowLeft").style.display = "block";
        document.querySelector(".border-next-button").style.display = "none";
        document.querySelector(".border-previous-button").style.display = "block";
    } else {
        document.querySelector(".arrowLeft").style.display = "block";
        document.querySelector(".arrowRight").style.display = "block";
        document.querySelector(".border-previous-button").style.display = "block";
        document.querySelector(".border-next-button").style.display = "block";
    }
}



