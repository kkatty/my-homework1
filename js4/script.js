//Функции нужны для того что бы вызывать одно и тоже действие n количество раз
//В функцию нужно передавать аргумент что бы можно было обратиться к этим аргументам внутри функции
//return завершает выполнение текущей функции и возвращает значение этой функции. когда вызывается return функция
//прекращается




function getAndCheckNumber(validNumber){
   let checkNum1 = validNumber;


    while (!checkNum1 || isNaN(checkNum1)){
       checkNum1 = parseInt(prompt('Enter the number'))
    }
    return checkNum1;
}


const num1 = getAndCheckNumber( +prompt(`Enter first number`));
const num2 = getAndCheckNumber (+prompt(`Enter second number`));

let mathoper = prompt('Enter math operation');

function mathOperation (sign, num1, num2){


    if (sign === '+') {
        return num1 + num2
    }
    else if (sign === '-') {
        return num1 - num2
    }
    else if (sign === '*') {
        return num1 * num2
    }
    else if (sign === '/') {
        return num1 / num2
    }
    else  {
        return 'I don"t know the operation'
    }

}

console.log(mathOperation(mathoper, num1, num2));
