
// типи даних : number, boolean, bigint, string, symbol, underfind, null, object
// == (рівність) приводить значення до одного типу даних, === (строга рівність) не приводить
// оператор - іункція що виповняє операцію над значеннями (математичну, логічну, порівняння, присвоєння)

"use strict";
let userName = prompt('Enter your name');
while (isFinite(userName) ) {
   userName = prompt('Enter your name');
}
let userAge = +prompt('Enter your age');
while (isNaN(userAge) ) {
    userAge = prompt('Enter your age');
}
if (userAge < 18) {
    confirm('You are not allowed to visit this website');
}
else if (userAge > 22 && userAge < 100) {
    confirm(`Welcome, ${userName}`);
}
else {

    if ( confirm('Are you sure you want to continue?') === true) {
        alert(`Welcome, ${userName}`);
    }
    else {
        alert('You are not allowed to visit this website');
    }
}