//setTimeout  вызвает функцию один раз через определённый интервал времени.
// setInterval вызывает функцию регулярно
//Планировщик будет вызывать функцию только после завершения выполнения кода. Но планировать вызов максимально быстро
//Потому что функция clearInterval() отменяет повторения действий, установленные вызовом функции setInterval().

let left = 0;
let timer;
sliderTimer();

function sliderTimer(){
timer = setTimeout(function (){
    const wrapper = document.querySelector('.container');
    left = left - 500;
    if (left < -1500){
        left = 0;
    }
    wrapper.style.left = left + 'px';
    sliderTimer();
}, 3000)
}

function stopTranslation(){
    let stopBtn = document.getElementById('stop');
    stopBtn.addEventListener('click',function (){
       clearTimeout(timer);
    })
let continueBtn = document.getElementById('continue');
    continueBtn.addEventListener('click',function (){
        timer = setTimeout(sliderTimer);
    })
}
stopTranslation()


