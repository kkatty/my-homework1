const name = document.querySelectorAll('.tabs-title');
const tabsItems = document.querySelectorAll('.tabs-item');
name.forEach(function (item) {
    item.addEventListener("click", function () {
        let currentName = item;
        let tabId = currentName.getAttribute('data-tab');
        let currentTab = document.querySelector(tabId);

if (!currentName.classList.contains('active')){
    name.forEach(function (item) {
        item.classList.remove('active');
    })
    tabsItems.forEach(function (item) {
        item.classList.remove('active');
    })

    currentName.classList.add('active');
    currentTab.classList.add('active');
}

    })

})