// Циклы нужны для того что бы кратко записывать действия которые повторяються много раз
//  while используем когда пишем простой цикл без итерации и инициализации
//  do while используем когда необходимо хотя бы одно выполнение цикла вне завимости от того вернет ли условие true
//  for используем когда хотим задать циклу дополнительные настройки
//  while (true) используем когда пишем бесконечный цикл
// Явное преобразование данных это когда преобразование выполняет программист вручную,
// неявное - преобразование между различными типами данных выполняется автоматически


'use strict';
let num = +prompt(`Enter your number`);

for (let figure = 0; num >= figure; figure++) {
    if (figure % 5 === 0 && figure / 5 >= 1) {

        console.log(figure);
    }
}

if (num <= 4 || isNaN(num)) {
    alert(`sorry, no numbers`);
}
while ( Number.isInteger(num) === false ){
     num = +prompt(`Enter your number`);
}



















