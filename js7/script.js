//выполняет функцию callback  для каждого элемента массива 1 раз
//установить его длину на ноль
//методом .isArray






const array = ['hello', 'world', 23, '23', true];

function filterBy(arr,type){
  return arr.filter (item => typeof item !== type)
}

console.log(filterBy(array,  "string"));
console.log(filterBy(array,  "number"));
console.log(filterBy(array,  "boolean"));
console.log(filterBy(array,  "object"))